Upstream: under review, https://github.com/mozilla/bleach/pull/565
Reason: Fix tests with python 3.9

From 45c611b08b35d13a4cae9996ffa6e26ad0018e93 Mon Sep 17 00:00:00 2001
From: Greg Guthe <gguthe@mozilla.com>
Date: Thu, 5 Nov 2020 10:28:07 -0500
Subject: [PATCH] sanitizer: update sanitize_uri_value for Python 3.9 urlparse

Use Python 3.9 urlparse scheme parsing behavior for all Python
versions

Changes:

* add utils._parse_uri_scheme to match Python 3.9 urlparse behavior
* add utils._is_valid_netloc_and_port with Django URL validator
* in test_uri_value_allowed_protocols:
  * add test case for implicit http for IP and port with path and fragment
  * add test case for data: scheme
  * add test case for relative path URI
  * test "is not allowed by default" test cases against default
    ALLOWED_PROTOCOLS
  * convert test_invalid_uri_does_not_raise_error into a test case
---
 bleach/sanitizer.py | 60 ++++++++++++++++++++++++--------------
 bleach/utils.py     | 70 +++++++++++++++++++++++++++++++++++++++++++++
 tests/test_clean.py | 41 +++++++++++++++++++++-----
 3 files changed, 143 insertions(+), 28 deletions(-)

diff --git a/bleach/sanitizer.py b/bleach/sanitizer.py
index bc66ad2..67e8700 100644
--- a/bleach/sanitizer.py
+++ b/bleach/sanitizer.py
@@ -9,7 +9,12 @@ from six.moves.urllib.parse import urlparse
 from xml.sax.saxutils import unescape
 
 from bleach import html5lib_shim
-from bleach.utils import alphabetize_attributes, force_unicode
+from bleach.utils import (
+    _is_valid_netloc_and_port,
+    _parse_uri_scheme,
+    alphabetize_attributes,
+    force_unicode,
+)
 
 
 #: List of allowed tags
@@ -443,9 +448,20 @@ class BleachSanitizerFilter(html5lib_shim.SanitizerFilter):
         return new_tokens
 
     def sanitize_uri_value(self, value, allowed_protocols):
-        """Checks a uri value to see if it's allowed
+        """Checks a URI value to see if it's allowed
+
+        ``urllib.parse.urlparse`` must be able to parse the URI.
+
+        The URI scheme must be in ``allowed_protocols`` or not have a
+        scheme and begin with a ``#`` indicating a relative URI by
+        fragment.
+
+        When ``"http"`` is in ``allowed_protocols`` (the default),
+        ``sanitize_uri_value`` also allows relative URIs matching an
+        IP address or hostname and port (e.g. ``localhost:8000``) and
+        relative URIs without a scheme (e.g. ``/path``).
 
-        :arg value: the uri value to sanitize
+        :arg value: the URI value to sanitize
         :arg allowed_protocols: list of allowed protocols
 
         :returns: allowed value or None
@@ -469,33 +485,35 @@ class BleachSanitizerFilter(html5lib_shim.SanitizerFilter):
         new_value = new_value.lower()
 
         try:
-            # Drop attributes with uri values that have protocols that aren't
-            # allowed
-            parsed = urlparse(new_value)
+            _ = urlparse(new_value)
         except ValueError:
             # URI is impossible to parse, therefore it's not allowed
             return None
 
-        if parsed.scheme:
-            # If urlparse found a scheme, check that
-            if parsed.scheme in allowed_protocols:
-                return value
+        # If there's no protocol/scheme specified, then assume it's "http"
+        # and see if that's allowed
+        implicit_http_allowed = "http" in allowed_protocols
 
+        # Drop attributes with uri values that have protocols that aren't
+        # allowed
+        scheme = _parse_uri_scheme(new_value)
+        if scheme:
+            if scheme in allowed_protocols:
+                return value
+            elif implicit_http_allowed and _is_valid_netloc_and_port(scheme):
+                return value
+            else:
+                # parsed a disallowed protocol/scheme
+                # or implicit protocols are allowed and it's an invalid netloc:port
+                return None
         else:
-            # Allow uris that are just an anchor
             if new_value.startswith("#"):
+                # Allow uris that are just an anchor
                 return value
-
-            # Handle protocols that urlparse doesn't recognize like "myprotocol"
-            if ":" in new_value and new_value.split(":")[0] in allowed_protocols:
-                return value
-
-            # If there's no protocol/scheme specified, then assume it's "http"
-            # and see if that's allowed
-            if "http" in allowed_protocols:
+            elif implicit_http_allowed:
                 return value
-
-        return None
+            else:
+                return None
 
     def allow_token(self, token):
         """Handles the case where we're allowing the tag"""
diff --git a/bleach/utils.py b/bleach/utils.py
index ad780d5..53f41b4 100644
--- a/bleach/utils.py
+++ b/bleach/utils.py
@@ -1,4 +1,5 @@
 from collections import OrderedDict
+import re
 
 import six
 
@@ -40,3 +41,72 @@ def force_unicode(text):
 
     # If not, convert it
     return six.text_type(text, "utf-8", "strict")
+
+
+# a subset of Django's URL validator
+#
+# from https://github.com/django/django/blob/3.1.3/django/core/validators.py#L63-L90
+
+# IP patterns
+ipv4_re = r"(?:25[0-5]|2[0-4]\d|[0-1]?\d?\d)(?:\.(?:25[0-5]|2[0-4]\d|[0-1]?\d?\d)){3}"
+ipv6_re = r"\[[0-9a-f:.]+\]"  # (simple regex, validated later)
+
+# Host patterns
+ul = ""  # "\u00a1-\uffff"  # Unicode letters range (must not be a raw string).
+
+hostname_re = r"[a-z" + ul + r"0-9](?:[a-z" + ul + r"0-9-]{0,61}[a-z" + ul + r"0-9])?"
+# Max length for domain name labels is 63 characters per RFC 1034 sec. 3.1
+domain_re = r"(?:\.(?!-)[a-z" + ul + r"0-9-]{1,63}(?<!-))*"
+tld_re = (
+    r"\."  # dot
+    r"(?!-)"  # can't start with a dash
+    r"(?:[a-z" + ul + "-]{2,63}"  # domain label
+    r"|xn--[a-z0-9]{1,59})"  # or punycode label
+    r"(?<!-)"  # can't end with a dash
+    r"\.?"  # may have a trailing dot
+)
+host_re = "(" + hostname_re + domain_re + tld_re + "|localhost)"
+
+netloc_re = r"(?:" + ipv4_re + "|" + ipv6_re + "|" + host_re + ")"  # network location
+port_re = r"(?::\d{2,5})?"  # port
+
+netloc_port_re = re.compile("^" + netloc_re + port_re + "$", re.IGNORECASE)
+
+
+# Characters valid in scheme names
+scheme_chars = (
+    "abcdefghijklmnopqrstuvwxyz" "ABCDEFGHIJKLMNOPQRSTUVWXYZ" "0123456789" "+-."
+)
+
+
+def _is_valid_netloc_and_port(netloc):
+    """
+    Returns the scheme for a URI or None when parsing the URI fails
+
+    :arg str/unicode netloc:
+
+    :returns: bool
+
+    """
+    return bool(netloc_port_re.match(netloc))
+
+
+def _parse_uri_scheme(uri):
+    """
+    Returns the scheme for a URI or None when parsing the URI fails
+
+    :arg str/unicode text:
+
+    :returns: text or None
+
+    """
+    # replicate Python 3.9 urlparse scheme parsing for older Python versions
+    i = uri.find(":")
+    if i > 0:
+        scheme = uri[:i]
+        for c in uri[:i]:
+            if c not in scheme_chars:
+                break
+        return scheme
+
+    return None
diff --git a/tests/test_clean.py b/tests/test_clean.py
index 1cd58df..c751661 100644
--- a/tests/test_clean.py
+++ b/tests/test_clean.py
@@ -6,7 +6,7 @@ import pytest
 
 from bleach import clean
 from bleach.html5lib_shim import Filter
-from bleach.sanitizer import Cleaner
+from bleach.sanitizer import ALLOWED_PROTOCOLS, Cleaner
 from bleach._vendor.html5lib.constants import rcdataElements
 
 
@@ -58,10 +58,6 @@ def test_html_is_lowercased():
     )
 
 
-def test_invalid_uri_does_not_raise_error():
-    assert clean('<a href="http://example.com]">text</a>') == "<a>text</a>"
-
-
 @pytest.mark.parametrize(
     "data, should_strip, expected",
     [
@@ -471,10 +467,31 @@ def test_attributes_list():
 @pytest.mark.parametrize(
     "data, kwargs, expected",
     [
+        # invalid URI (urlparse raises a ValueError: Invalid IPv6 URL)
+        # is not allowed by default
+        (
+            '<a href="http://example.com]">text</a>',
+            {"protocols": ALLOWED_PROTOCOLS},
+            "<a>text</a>",
+        ),
+        # data protocol is not allowed by default
+        (
+            '<a href="data:text/javascript,prompt(1)">foo</a>',
+            {"protocols": ALLOWED_PROTOCOLS},
+            "<a>foo</a>",
+        ),
         # javascript: is not allowed by default
-        ("<a href=\"javascript:alert('XSS')\">xss</a>", {}, "<a>xss</a>"),
+        (
+            "<a href=\"javascript:alert('XSS')\">xss</a>",
+            {"protocols": ALLOWED_PROTOCOLS},
+            "<a>xss</a>",
+        ),
         # File protocol is not allowed by default
-        ('<a href="file:///tmp/foo">foo</a>', {}, "<a>foo</a>"),
+        (
+            '<a href="file:///tmp/foo">foo</a>',
+            {"protocols": ALLOWED_PROTOCOLS},
+            "<a>foo</a>",
+        ),
         # Specified protocols are allowed
         (
             '<a href="myprotocol://more_text">allowed href</a>',
@@ -494,6 +511,11 @@ def test_attributes_list():
             '<a href="#example.com">foo</a>',
         ),
         # Allow implicit http if allowed
+        (
+            '<a href="/path">valid</a>',
+            {"protocols": ["http"]},
+            '<a href="/path">valid</a>',
+        ),
         (
             '<a href="example.com">valid</a>',
             {"protocols": ["http"]},
@@ -524,6 +546,11 @@ def test_attributes_list():
             {"protocols": ["http"]},
             '<a href="192.168.100.100:8000">valid</a>',
         ),
+        (
+            '<a href="192.168.100.100:8000/foo#bar">valid</a>',
+            {"protocols": ["http"]},
+            '<a href="192.168.100.100:8000/foo#bar">valid</a>',
+        ),
         # Disallow implicit http if disallowed
         ('<a href="example.com">foo</a>', {"protocols": []}, "<a>foo</a>"),
         ('<a href="example.com:8000">foo</a>', {"protocols": []}, "<a>foo</a>"),
-- 
2.30.0.rc1

