# Copyright 2013 Benedikt Morbach <moben@exherbo.org>
# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'pylint-1.7.2.ebuild' from Gentoo, which is:
#     Copyright 1999-2017 Gentoo Foundation

require pypi
require setup-py [ blacklist=2 import=setuptools has_bin=true test=pytest ]

SUMMARY="a python code static checker"
DESCRIPTION="
Pylint is a Python source code analyzer which looks for programming errors, helps enforcing a coding
standard and sniffs for some code smells (as defined in Martin Fowler's Refactoring book).

It's highly configurable and handle pragmas to control it from within your code.
Additionally, it is possible to write plugins to add your own checks.
"
HOMEPAGE="https://www.pylint.org/"

LICENCES="|| ( GPL-2 GPL-3 )"
SLOT="0"

MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/astroid[~>2.5.1][python_abis:*(-)?]
        dev-python/isort[>=4.2.5&<6][python_abis:*(-)?]
        dev-python/mccabe[~>0.6.0][python_abis:*(-)?]
        dev-python/toml[>=0.7.1][python_abis:*(-)?]
    test:
        dev-python/pytest-benchmark[python_abis:*(-)?]
        dev-python/pytest-runner[python_abis:*(-)?]
"

# those contain errors for testing pylint. Obviously, those are breaking byte-compilation
PYTHON_BYTECOMPILE_EXCLUDES=( test/functional test/input test/regrtest_data )

test_one_multibuild() {
    local disabled_tests=(
        # fails when HOME == TEMP
        "not test_pylint_home"
        # should be installed to load reporter plugins
        "and not test_version"
        "and not test_display_results_is_renamed"
    )
    export PYTEST_PARAMS=(
        --ignore=pylint/ # do not pick duplicates for files in PYTHONPATH
        -k "${disabled_tests[*]}"
    )

    esandbox allow_net "unix:${TEMP%/}/pymp-*/listener-*"
    esandbox allow_net --connect "unix:${TEMP%/}/pymp-*/listener-*"

    setup-py_test_one_multibuild

    esandbox disallow_net "unix:${TEMP%/}/pymp-*/listener-*"
    esandbox disallow_net --connect "unix:${TEMP%/}/pymp-*/listener-*"
}

