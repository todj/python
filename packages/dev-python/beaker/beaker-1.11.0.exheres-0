# Copyright 2008, 2009 Ali Polatel
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'beaker-0.9.4.ebuild' from Gentoo, which is:
#   Copyright 1999-2008 Gentoo Foundation

require pypi [ pnv=${PNV^} ] setup-py [ import=setuptools work=${PNV^} ]

SUMMARY="A simple WSGI middleware to use the Myghty Container API"
DESCRIPTION="
Beaker is a caching library that includes Session and Cache objects built on
Myghty's Container API used in MyghtyUtils. WSGI middleware is also included to
manage Session objects and signed cookies.
Beaker caching is implemented with namespaces allowing one to store not only any
Python data that can be pickled, but also multiple versions of it by using
multiple keys for a single piece of data under a namespace.
"
HOMEPAGE+=" https://github.com/bbangert/beaker"

UPSTREAM_DOCUMENTATION="
    https://beaker.readthedocs.io/en/latest/ [[ lang = en ]]
"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        python_abis:2.7? ( dev-python/funcsigs[python_abis:*(-)?] )
"

# the testsuite is currently not included in the tarball
# see https://github.com/bbangert/beaker/issues/108
RESTRICT="test"

