# Copyright 2008, 2009 Ali Polatel
# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'kid-0.9.6.ebuild' from Gentoo, which is:
#   Copyright 1999-2008 Gentoo Foundation

require pypi
require setup-py [ import='setuptools' blacklist='3' ]

SUMMARY="A simple and Pythonic XML template language"
DESCRIPTION="
Kid is a simple template language for XML based vocabularies written in Python.
It was spawned as a result of a kinky love triangle between XSLT, TAL, and PHP.
"

HOMEPAGE="http://www.kid-templating.org/"
LICENCES="MIT"

UPSTREAM_CHANGELOG=""
UPSTREAM_RELEASE_NOTES="http://www.kid-templating.org/notes.html"

PLATFORMS="~amd64 ~x86"
SLOT="0"
MYOPTIONS="doc examples"

DEPENDENCIES="
    build:
        doc? ( dev-python/docutils[python_abis:*(-)?] )
"

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( HISTORY RELEASING )

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild
    for file in bin/*;do
        edo sed -i "$file"  \
                -e "s|#!.*python|#!/usr/$(exhost --target)/bin/${PYTHON}|"
    done
}

compile_one_multibuild() {
    setup-py_compile_one_multibuild
    if option doc; then
        emake -C doc || die "emake doc failed"
    fi
}

test_one_multibuild() {
    # We have to move the build folder out of the way to make the tests work
    mv build build.bak
    PYTHONPATH="." "${PYTHON}" run_tests.py -x || die "tests failed"
    mv build.bak build
}

install_one_multibuild() {
    setup-py_install_one_multibuild

    dobin bin/*

    dodoc doc/*.txt
    if option doc; then
        insinto /usr/share/doc/${PNVR}/html
        doins doc/*.{html,css}
    fi

    if option examples ; then
        insinto /usr/share/doc/${PNVR}
        doins -r examples
    fi
}

