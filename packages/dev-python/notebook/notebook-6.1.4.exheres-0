# Copyright 2018 Alexander Kapshuna <kapsh@kap.sh>
# Distributed under the terms of the GNU General Public License v2

require pypi utf8-locale setup-py [ blacklist=2 import=setuptools test=nose ]

SUMMARY="A web-based notebook environment for interactive computing"
HOMEPAGE="https://jupyter-notebook.readthedocs.io/"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/argon2_cffi[python_abis:*(-)?]
        dev-python/ipykernel[python_abis:*(-)?]
        dev-python/ipython_genutils[python_abis:*(-)?]
        dev-python/Jinja2[python_abis:*(-)?]
        dev-python/jupyter_client[>=5.3.4][python_abis:*(-)?]
        dev-python/jupyter_core[>=4.6.1][python_abis:*(-)?]
        dev-python/nbconvert[python_abis:*(-)?]
        dev-python/nbformat[python_abis:*(-)?]
        dev-python/prometheus_client[python_abis:*(-)?]
        dev-python/pyzmq[>=17][python_abis:*(-)?]
        dev-python/Send2Trash[python_abis:*(-)?]
        dev-python/terminado[>=0.8.3][python_abis:*(-)?]
        dev-python/tornado[>=5.0][python_abis:*(-)?]
        dev-python/traitlets[>=4.2.1][python_abis:*(-)?]
    test:
        dev-python/coverage[python_abis:*(-)?]
        dev-python/nbval[python_abis:*(-)?]
        dev-python/nose-exclude[python_abis:*(-)?]
        dev-python/nose_warnings_filters[python_abis:*(-)?]
        dev-python/requests[python_abis:*(-)?]
"

pkg_setup() {
    require_utf8_locale
}

# They run tests on Selenium.
RESTRICT="test"

